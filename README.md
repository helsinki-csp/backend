# CSP Traffic Backend
The backend puzzle piece for counting people in public
transport in the HSL area. 

## Building
Navigate into the project directory and run the snippet below.
Maven should handle the installation for you.
```
mvn install compile
```

## Running / Testing
To run the project, use the below command.
```
mvn exec:java
```

Navigate to [localhost:4567](http://localhost:4567) and the application
should redirect you to a debugging view.
Please note that there is not much to see unless
you send in some test data yourself.

## Documentation
You can compile `seqdia.txt` into a sequence diagram
at [sequencediagram.org](https://sequencediagram.org/).
The diagram describes how our backend communicates
with the client.
function read()
{
    var request = new XMLHttpRequest();
    var current;

    request.onreadystatechange = function()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            message = JSON.parse(this.responseText);
            for (var i = 0; i < message.length; i++)
            {
                if (message[i]["internalTime"] > up)
                {
                    current = message[i];
                    times.push(current["internalTime"]);
                    messages[current["internalTime"]] = current;
                }
            }
            loop();
        }
    };

    request.open("GET", "/dump", true);
    request.send();
}

function loop()
{
    if (times.length > 0)
    {
        times = times.sort();
        up = times[(times.length - 1)];

        for (var i = 0; i < times.length; i++)
            show(messages[times[i]]);
    }
    messages = { };
    times = [ ];
}

function show(log)
{
    var node = document.createElement("div");
    node.className = log["type"];

    node.appendChild(
        document.createTextNode(log["stamp"] + " " + log["message"]));
    body.insertBefore(node, body.firstChild);
}

var up = 0;
var messages = { };
var times = [ ];

var body = document.querySelector("#watch-body");

setInterval(read, 2500);
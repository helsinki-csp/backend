package csp.traffic.domain;

public class Beacon
{

    // POJO presentation of the necessary
    // data retrieved from the beacon API

    // Identifying the beacon
    public String major;
    public String minor;

    // Type of beacon (stop, vehicle)
    public int location;

    // ID of stop
    public String stop;

    // ID of vehicle
    public String vehicle_number;
    public String operator;

    public void fixLeadingZeroes()
    {
        major = major.replaceFirst("^0+", "");
        minor = minor.replaceFirst("^0+", "");
    }

    public String getIdentity()
    {
        return String.format("%s:%s",
            major, minor);
    }

}

package csp.traffic.domain;

public class Entity
{

    private final String unique;
    private final String beacon;

    public Entity(String unique, String beacon)
    {
        this.unique = unique;
        this.beacon = beacon;
    }

    public String getIdentifier()
    {
        return unique;
    }

    public String getBeaconIdentifier()
    {
        return beacon;
    }

    @Override
    public int hashCode()
    {
        return (31 * unique.hashCode() + 7);
    }

}

package csp.traffic.domain;

public class Person
{

    private final String id;
    private long internalTime;

    public Person(String id, long internalTime)
    {
        this.id = id;
        this.internalTime = internalTime;
    }

    public String getId()
    {
        return id;
    }

    public long getActivity()
    {
        return internalTime;
    }

    public void setActivity(long internalTime)
    {
        this.internalTime = internalTime;
    }

}

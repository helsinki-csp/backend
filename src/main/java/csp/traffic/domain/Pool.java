package csp.traffic.domain;

import csp.traffic.ClockUtil;
import csp.traffic.log.Log;
import csp.traffic.log.Logger;
import java.util.Date;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class Pool
{

    private final int INVALIDATE_MINUTES = 1;
    private final String beacon;

    private final Set<Person> people
        = ConcurrentHashMap.newKeySet();

    public Pool(String beacon)
    {
        this.beacon = beacon;
    }

    public int getAmount()
    {
        invalidateOld();
        return people.size();
    }

    public int addReport(Person person, Entity entity)
    {
        Person existing = findPerson(person.getId());

        if (existing == null)
        {
            people.add(person);
            existing = person;
        }

        existing.setActivity(
            System.currentTimeMillis());

        Logger.message(Log.LogType.REPORT, String.format
        (
            "Incoming report (%s, (%s -> %s), %d)",
            person.getId(), entity.getBeaconIdentifier(),
            entity.getIdentifier(), people.size())
        );

        return people.size();
    }

    private Person findPerson(String uuid)
    {
        return people.stream().filter(p -> p.getId().hashCode() == uuid.hashCode())
            .findFirst().orElse(null);
    }

    private void invalidateOld()
    {
        Date compareTime = ClockUtil.currentTime();
        int oldSize = people.size();

        for (Person person : people)
        {
            if (compareTime.after(ClockUtil.basePlus(person
                .getActivity(), INVALIDATE_MINUTES)))
            {
                people.remove(person);
            }
        }

        int difference = (oldSize - people.size());
        if (difference > 0)
        {
            Logger.message(Log.LogType.REPORT, String.format("Cleaned up %d people (%s, %d)",
                difference, beacon, people.size()));
        }
    }

}

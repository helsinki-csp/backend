package csp.traffic;

import csp.traffic.log.Log;
import csp.traffic.log.Logger;
import csp.traffic.service.OccupancyService;
import java.util.UUID;
import static spark.Spark.*;

public class Application
{

    public static void main(String... arg)
    {
        Logger.message(Log.LogType.CODE,
            "Application started");

        port(getHerokuAssignedPort());

        staticFiles.location("/");
        OccupancyService.initialize();

        String secret = UUID.randomUUID().toString();
        System.out.println(String.format(
            "!!! Reboot the system by accessing %s !!!", secret));

        // API routes

        post("/auth", // Authorize user
            Controller::routeAuth);

        post("/beac", // Handle report about beacon
            Controller::routeBeac);

        post("/occu", // Collect occupancy data
            Controller::routeOccu);

        // GET routes

        get("/dump", // Fetch log messages as JSON
            Controller::routeDump);

        get(String.format("/%s", secret),
            Controller::routeInit);

        get("*", (req, res) ->
        {
            res.redirect("debug.html");
            return null;
        });
    }

    static int getHerokuAssignedPort()
    {
        ProcessBuilder process = new ProcessBuilder();
        if (process.environment().get("PORT") != null)
        {
            return Integer.parseInt(process.environment().get("PORT"));
        }
        return 4567;
    }

}

package csp.traffic.log;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedDeque;

public class Logger
{

    private static final ConcurrentLinkedDeque<Log> log
        = new ConcurrentLinkedDeque<>();
    private static final int LOGGER_SIZE = 100;

    public static void message(Log.LogType type, String message)
    {
        if (log.size() > LOGGER_SIZE) log.remove();

        log.add(new Log(type, message, System.nanoTime(),
            new SimpleDateFormat("[dd/MM HH:mm:ss]").format(new Date())));
    }

    public static List<Object> read()
    {
        return Arrays.asList(log.toArray());
    }

}

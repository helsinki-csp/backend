package csp.traffic.log;

public class Log
{

    private final LogType type;
    private final String message;

    private final long internalTime;
    private final String stamp;

    public Log(LogType type, String message, long internalTime, String stamp)
    {
        this.type = type;
        this.message = message;

        this.internalTime = internalTime;
        this.stamp = stamp;
    }

    public enum LogType
    {
        USER, QUERY, REPORT, CODE
    }

}

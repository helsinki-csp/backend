package csp.traffic.service;

import csp.traffic.ClockUtil;
import csp.traffic.domain.Person;
import csp.traffic.log.Log;
import csp.traffic.log.Logger;
import java.util.Date;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public abstract class UserService
{

    private static final int INVALIDATE_MINUTES = 5;

    private static final Set<Person> people
        = ConcurrentHashMap.newKeySet();

    // Grant a new UUID, add to database
    public static String grantUUID()
    {
        String uuid = UUID.randomUUID().toString();
        people.add(new Person(uuid, System.currentTimeMillis()));
        Logger.message(Log.LogType.USER,
            String.format("Authorized %s", uuid));
        return uuid;
    }

    public static boolean isValid(String uuid)
    {
        invalidateOld();
        return ((findPerson(uuid) == null) ? false : true);
    }

    public static void invalidateAll()
    {
        Logger.message(Log.LogType.USER, "Invalidated everyone");
        people.clear();
    }

    public static Person findPerson(String uuid)
    {
        return people.stream().filter(p -> p.getId().hashCode() == uuid.hashCode())
            .findFirst().orElse(null);
    }

    private static void invalidateOld()
    {
        Date compareTime = ClockUtil.currentTime();
        for (Person person : people)
        {
            if (compareTime.after(ClockUtil.basePlus(person
                .getActivity(), INVALIDATE_MINUTES)))
            {
                Logger.message(Log.LogType.USER, String.format(
                    "Invalidated %s", person.getId()));
                people.remove(person);
            }
        }
    }

}

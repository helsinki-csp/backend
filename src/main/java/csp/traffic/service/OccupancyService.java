package csp.traffic.service;

import com.google.gson.Gson;
import csp.traffic.domain.Beacon;
import csp.traffic.domain.Entity;
import csp.traffic.domain.Pool;
import csp.traffic.log.Log;
import csp.traffic.log.Logger;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;

public abstract class OccupancyService
{

    private static final String BEACON_API = "https://dev.hsl.fi/beacons";
    private static final Gson gson = new Gson();

    private static final Map<Entity, Pool> database
        = new ConcurrentHashMap<>();

    // Populates the in-memory database
    // with values from the API
    public static void initialize()
    {
        try
        {
            Logger.message(Log.LogType.CODE, "Initializing database...");
            database.clear();

            URL address = new URL(BEACON_API);
            try (InputStreamReader reader = new InputStreamReader(address.openStream()))
            {
                Beacon[] content = gson.fromJson(reader, Beacon[].class);
                for (Beacon beacon : content)
                {
                    beacon.fixLeadingZeroes();
                    database.put(new Entity(parseEntity(beacon),
                        beacon.getIdentity()), new Pool(beacon.getIdentity()));
                }
                Logger.message(Log.LogType.CODE, "Database initialized");
            }
        }
        catch (Exception ex)
        {
            Logger.message(Log.LogType.CODE, String.format(
                "Database initialization failed with message: %s", ex.getMessage()));
        }
    }

    // Allow people to send a status
    // update of their location
    public static int addReport(String person, String beacon)
    {
        Entity entity = findEntityByBeacon(
            removeLeadingZeroes(beacon));

        if (entity == null)
            return -1;

        return database.get(entity).addReport(UserService
            .findPerson(person), entity);
    }

    // Collect occupancy data by HSL IDs
    public static Map<String, Integer> collectOccupancy(List<String> entities)
    {
        Map<String, Integer> occupancy = new HashMap<>();
        for (String queried : entities)
            occupancy.put(queried, getValue(findEntity(queried)));

        if (occupancy.size() > 0)
            Logger.message(Log.LogType.QUERY, String.format(
                "Responded to query (%s)", occupancy));

        return occupancy;
    }

    private static Entity findEntity(Predicate<Entity> filter, Entity backup)
    {
        return database.keySet().stream().filter(filter)
            .findFirst().orElse(backup);
    }

    private static Entity findEntity(String id)
    {
        Entity entity = new Entity(id, "0");
        return findEntity((e -> e.hashCode() == entity
            .hashCode()), entity);
    }

    private static Entity findEntityByBeacon(String beacon)
    {
        return findEntity((e -> e.getBeaconIdentifier()
            .hashCode() == beacon.hashCode()), null);
    }

    private static int getValue(Entity entity)
    {
        Pool pool = database.get(entity);
        return (pool == null)
            ? -1 : pool.getAmount();
    }

    // Return unique ID of the entity associated
    // with a specific beacon
    private static String parseEntity(Beacon beacon)
    {
        if (beacon.location == 1)
            return (String.format("%s:%s", beacon.operator,
                beacon.vehicle_number));
        return beacon.stop;
    }

    private static String removeLeadingZeroes(String beacon)
    {
        String[] pieces = beacon.split(":");
        if (pieces[0] == null || pieces[1] == null)
            return "";

        return String.format
        (
            "%s:%s",
            pieces[0].replaceFirst("^0+", ""),
            pieces[1].replaceFirst("^0+", "")
        );
    }

}

package csp.traffic;

import com.google.gson.Gson;
import csp.traffic.log.Log;
import csp.traffic.log.Logger;
import csp.traffic.service.OccupancyService;
import csp.traffic.service.UserService;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

public abstract class Controller
{

    static final Gson gson = new Gson();

    public static String routeAuth(spark.Request req, spark.Response res)
    {
        String uuid = UserService.grantUUID();
        if (!UserService.isValid(uuid))
            return "-1";
        return uuid;
    }

    public static String routeBeac(spark.Request req, spark.Response res)
    {
        try
        {
            String[] received = URLDecoder.decode(req.body(),
                "UTF-8").split("&");

            String person = received[0].split("=")[1];
            String report = received[1].split("=")[1];

            if (UserService.isValid(person))
                return Integer.toString(OccupancyService.addReport(person, report));
        }
        catch (Exception ex) { }
        return "-1";
    }

    public static String routeOccu(spark.Request req, spark.Response res)
    {
        List<String> entities = new ArrayList<>();

        try
        {
            // Add all values to a list
            for (String entity : URLDecoder.decode(req.body(),
                "UTF-8").split("&"))
            {
                entities.add(entity.split("=")[0]);
            }
        }
        catch (Exception ex) { }

        // Return values associated per ID in list
        return gson.toJson(OccupancyService
            .collectOccupancy(entities));
    }

    public static String routeDump(spark.Request req, spark.Response res)
    {
        return gson.toJson(Logger.read());
    }

    public static String routeInit(spark.Request req, spark.Response res)
    {
        Logger.message(Log.LogType.CODE, "REBOOT REQUESTED");
        UserService.invalidateAll();
        OccupancyService.initialize();

        res.redirect("debug.html");
        return null;
    }

}

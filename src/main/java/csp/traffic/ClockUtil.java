package csp.traffic;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public abstract class ClockUtil
{

    public static Date currentTime()
    {
        return (new Date(System.currentTimeMillis()));
    }

    public static Date basePlus(long base, long plus)
    {
        return (new Date(base + TimeUnit.MINUTES
            .toMillis(plus)));
    }

}
